#[macro_use]
extern crate clap;

use std::fs::File;
use std::io;
use std::io::prelude::*;

fn main() -> Result<(), io::Error>{
    let matches = clap_app!(rustycat =>
        (version: "0.1.0")
        (author: "Tyler Goodwin")
        (about: "Garbage attempt at a Cat clone")
        (@arg INPUT: +required ... "Sets the input files to use")
    ).get_matches();

    let files = matches.values_of("INPUT").unwrap();
    
    for file in files {
        match file {
            "-" => read_from_std_in(),
            _ => read_and_output(file), 
        }?;
    }
    Ok(())
}

fn read_and_output(filename : &str) -> Result<(), io::Error>{
    let mut f = File::open(filename)?;

    let mut contents = String::new();
    f.read_to_string(&mut contents)?;

    print!("{}", contents);
    Ok(())
}

fn read_from_std_in() -> Result<(), io::Error>{
    let mut buffer = String::new();
    io::stdin().read_to_string(&mut buffer)?;
    print!("{}", buffer);
    Ok(())
}